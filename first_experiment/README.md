# CodeCity: On-Screen or in Virtual Reality? - Replication Package

The paper is available in [paper.pdf](./paper.pdf) file.

## Table of Contents

A high level of this reproduction package doc.

- [Task ids](#tasks-ids)
- [Scenes of the experiment](#scenes-of-the-experiment)
- [Participants questionnaire](#participants-questionnaire)
- [Answer References](#answers-references)
- [Results data](#results-data)
- [Results analysis](#results-analysis)


## Tasks ids

**IMPORTANT**: In order to alleviate the confusion in the paper and a better analysis, the tags shown in the experiment are different that the ones described in the experiment proposed to the participant, here is the table of the correlations between IDs:

| **Paper ID** | **First experiment Replication Package ID** |
|:------------:|:-------------------------------------------:|
|     T1_*     |                    E1 T1                    |
|     T2_*     |                    E1 T2                    |
|     T3_*     |                    E1 T3                    |
|     T4_*     |                    E1 T4                    |
|    T5_1_*    |                    E1 T5                    |
|    T5_2_*    |                    E1 T6                    |
|    T5_3_*    |                    E1 T7                    |
|    T5_4_*    |                    E1 T8                    |
|     CORE     |                    E1 T11                   |


## Scenes of the experiment

Inside the `scenes` folder, there are all the needed files for creating the scenes presented to the participants. The entrypoint for the replicant should be the file `scenes/index.html` file, which contains all the needed links of the scenes for doing the experiment.

[Go here to see the scenes](https://thesis-dlumbrer.gitlab.io/is21-reprpckg/first_experiment/scenes/index.html)

### Deploy

The easiest way to do it is to go to the `scenes` folder and deploy the `scenes/index.html` file within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```

Please, make sure that the http server has been deployed successfully (references [1] [2] and [3] for the examples described). Once deployed, you will see the next page with links to the specific scenes presented to the participants:

![page](./scenes/assets/indexpage.png)

Click on the links to get redirected to the specific scenes.

[1]: https://www.npmjs.com/package/http-server
[2]: https://docs.python.org/2/library/simplehttpserver.html
[3]: https://docs.python.org/3/library/http.server.html#http.server.SimpleHTTPRequestHandler

### Specific scenes

These are the HTML files with the specific scenes for each type of participant (VR and On-Screen), the replicant should be able to visit them through the links of the `scenes/index.html` page (visit [Deploy](#Deploy) section):

- `scenes/screen/jetuml2021.html`: Scene for the On-Screen participants for the JetUML version as of March 24, 2021
- `scenes/screen/jetuml2018.html`: Scene for the On-Screen participants for the JetUML version as of June 28, 2018
- `scenes/vr/jetuml2021.html`: Scene for the VR participants for the JetUML version as of March 24, 2021
- `scenes/screen/jetuml2018.html`: Scene for the VR participants for the JetUML version as of June 28, 2018

You can deploy each html isolated using a simple http server on each path.

## Participants questionnaire

Inside the `questionnaire` folder there are in different file format, the questionnaire presented to the participants:

- [Markdown version](./questionnaire/QUESTIONNAIRE.md)
- [PDF version](./questionnaire/QUESTIONNAIRE.pdf)
- [HTML version](./questionnaire/QUESTIONNAIRE.html)

## Answers references

Inside the `references` folder there is a CSV file (`references.csv`) with the right answers to the tasks, to take it as a reference. In each file search task (except T1 and T5.1), the participant must search for the top 3 files, in this reference file up to the top 5 files are included in order to compare in a higher rank.

[Click here to go to the file](./references/reference.csv)

### Columns description

- **T1_location**: Answer to the question of Task 1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T2_file1**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T2_file2**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T2_file3**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T2_file4**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fourth file with the highest number of functions**
- **T2_file5**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fifth file with the highest number of functions**
- **T3_file1**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T3_file2**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T3_file3**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T3_file4**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fourth file with the highest lines of code per function**
- **T3_file5**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fifth file with the highest lines of code per function**
- **T4_file1**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T4_file2**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T4_file3**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T4_file4**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fourth file with the highest ccn**
- **T4_file5**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fifth file with the highest ccn**
- **T5_1_location**: Answer to the question of Task 5.1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T5_2_file1**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T5_2_file2**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T5_2_file3**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T5_2_file4**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fourth file with the highest number of functions**
- **T5_2_file5**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fifth file with the highest number of functions**
- **T5_3_file1**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T5_3_file2**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T5_3_file3**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T5_3_file4**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fourth file with the highest lines of code per function**
- **T5_3_file5**: Answer to the question of Task 5,3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fifth file with the highest lines of code per function**
- **T5_4_file1**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T5_4_file2**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T5_4_file3**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T5_4_file4**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fourth file with the highest ccn**
- **T5_4_file5**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fifth file with the highest ccn**


## Results data

In the `results` folder there are the CSV files of the answers (raw and formatted) of the participants:

- `results/results_screen_raw.csv`: Contains the raw answers of the screen participants. [Click here to go to the file](./results/results_screen_raw.csv)
- `results/results_screen_formatted.csv`: Contains the formatted answers of the screen participants. [Click here to go to the file](./results/results_screen_formatted.csv)
- `results/results_vr_raw.csv`: Contains the raw answers of the VR participants. [Click here to go to the file](./results/results_vr_raw.csv)
- `results/results_vr_formatted.csv`: Contains the formatted answers of the VR participants. [Click here to go to the file](./results/results_vr_formatted.csv)

The formatted files are the same results but with the fields formatted for the comparison between VR and Screen participants.

### Columns description

- **AGE**: Answer to the question of the Demographics survey "Age (for statistical purposes only)"
- **JOB_POSTION**: Answer to the question of the Demographics survey "Job position (i.e, developer, researcher, project manager, etc.)"
- **EXP_OOP**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Object-oriented programming"
- **EXP_PRP**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Procedural programming"
- **EXP_FNP**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Functional programming"
- **EXP_REV**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Reverse engineering"
- **EXP_IDE**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Using a programming IDE (Pycharm, Eclipse, Visual Studio)"
- **YEARS__OOP**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-5, 7-10, 10+): Object-oriented programming"
- **YEARS__PRP**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-5, 7-10, 10+): Procedural programming"
- **YEARS__FNP**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-5, 7-10, 10+): Functional programming"
- **YEARS__REV**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-5, 7-10, 10+): Reverse engineering"
- **YEARS__IDE**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-5, 7-10, 10+): Using a programming IDE (Pycharm, Eclipse, Visual Studio)"
- **FAM_JETUML**: Answer to the question of the Demographics survey "Are you familiar with JetUML?"
- **VR_HEADSET**: Answer to the question of the Demographics survey "Have you ever used a Virtual Reality headset (e.g., Oculus)?"
- **T1_location**: Answer to the question of ask 1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T1_dif**: Answer to the question of the Task 1 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T1_dt**: Total time spent completing Task 1
- **T2_file1**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T2_file2**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T2_file3**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T2_dif**: Answer to the question of Task 2 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T2_dt**: Total time spent completing Task 2
- **T3_file1**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T3_file2**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T3_file3**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T3_dif**: Answer to the question of Task 3 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T3_dt**: Total time spent completing Task 3
- **T4_file1**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T4_file2**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T4_file3**: Answer to the question of Task 4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T4_dif**: Answer to the question of Task 4 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T4_dt**: Total time spent completing Task 4
- **T5_1_location**: Answer to the question of Task 1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T5_1_dif**: Answer to the question of Task 5.1 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T5_1_dt**: Total time spent completing Task 5.1
- **T5_2_file1**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T5_2_file2**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T5_2_file3**: Answer to the question of Task 5.2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T5_2_dif**: Answer to the question of Task 5.2 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T5_2_dt**: Total time spent completing Task 5.2
- **T5_3_file1**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T5_3_file2**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T5_3_file3**: Answer to the question of Task 5.3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T5_3_dif**: Answer to the question of Task 5.3 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T5_3_dt**: Total time spent completing Task 5.3
- **T5_4_file1**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T5_4_file2**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T5_4_file3**: Answer to the question of Task 5.4 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T5_4_dif**: Answer to the question of Task 5.4 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T5_4_dt**: Total time spent completing Task 5.4
- **CORE**: Answer to the question of the Final Survey "After your experience with the cities and the metrics that it shows, identify what represents for you the core of the JetUML project. Select the location and say where it is."
- **UNDERSTANDING**: Answer to the question of the Final Survey "Did you find it difficult to understand the experiment? (In that case, why?)"
- **DEVICE**: Answer to the question of the Final Survey "What device did you use for the experiment? (i.e. computer, Oculus, etc)."
- **SUGGESTION**: Answer to the question of the Final Survey "Any other suggestion/comment?"

### Results analysis

The analysis presented in the article, and the figures that are there were generated using a Jupyter notebook included in the `analysis` folder (`analysis.ipynb`), you can see the result of the analysis in the following formats:

- [Python notebook](./analysis/analysis.ipynb): `./analysis/analysis.ipynb` (if you want to execute it, you need to install `jupyter-lab`, `pandas`, `matplotlib` and `scipy`)
- [Markdown](./analysis/analysis.md): `./analysis/analysis.md`
- [PDF](./analysis/analysis.pdf): `./analysis/analysis.pdf`
- [HTML](./analysis/analysis.html): `./analysis/analysis.html`


All the figures and tables have been used in the article.