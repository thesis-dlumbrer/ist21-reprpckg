# Questionnaire presented to the participants

It includes the demographics survey, the tasks survey, and the feedback survey.

## Demographics

- How old are you?
- Job position (e.g., developer, researcher, project manager)
- Experience level in Object-Oriented programming
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert
- Experience level in Procedural programming
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert
- Experience level in Functional programming
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert
- Experience level in Reverse engineering
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert
- Experience level in using an IDE (Pycharm, Eclipse, Visual Studio, etc.)
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert
- Object-oriented programming experience (in years)
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10
- Procedural programming experience (in years)
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10
- Functional programming experience (in years)
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10
- Number of years of Reverse Engineering
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10
- How long have you been using an IDE (in years)?
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10
- Are you familiar with the JetUML system (https://github.com/prmr/JetUML)?
    - [ ] Yes
    - [ ] No
- Do you have experience with VR devices? (*This was just presented to the VR participants*)

## Tasks

You are going to visit a city that represents the JetUML project (https://github.com/prmr/JetUML) updated as of March 24, 2021. Each building represents a file in the source code of the system. Each quarter (or district) represents a folder. Quarters can be nested to represent nested folders. On each building, we mapped the values of three software metrics:

- Area: number of functions (num_funs) of the code file. 
- Height: Lines of Code (LOC) per function. 
- Color*: Cyclomatic Complexity Number (see https://en.wikipedia.org/wiki/Cyclomatic_complexity)

* The color follows a heatmap scale from blue (low) to red (high).

Scene: `scenes/(screen or vr)/jetuml2021.html`


- **Task 1**: Explore the city to locate all the test code (i.e., files and directories) of the system
    - Where is the location of the test files? (e.g., "The folder named "junit" that is located on the East side of the city)
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 2**: Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system
    - First source code file with the highest number of functions
    - Second source code file with the highest number of functions
    - Third source code file with the highest number of functions
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 3**: Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system
    - First source code file with the highest lines of code per function
    - Second source code file with the highest lines of code per function
    - Third source code file with the highest lines of code per function
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 4**: Find the three source code files (not testing files) with the highest Cyclomatic Complexity Number (highest ccn)  
    - First source code file with the highest ccn
    - Second source code file with the highest ccn
    - Third source code file with the highest ccn
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

### To solve the next task, we need to go back in time. The next scene will be a representation of an older snapshot of JetUML, specifically Release 2.1 released on June 28, 2018.

Scene: `scenes/(screen or vr)/jetuml2018.html`

- **Task 5.1**: Explore the city to locate all the test code (i.e., files and directories) of the system
    - Where is the location of the test files? (e.g., "The folder named "junit" that is located on the East side of the city)
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 5.2**: Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system
    - First source code file with the highest number of functions
    - Second source code file with the highest number of functions
    - Third source code file with the highest number of functions
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 5.3**: Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system
    - First source code file with the highest lines of code per function
    - Second source code file with the highest lines of code per function
    - Third source code file with the highest lines of code per function
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree
- **Task 5.4**: Find the three source code files (not testing files) with the highest Cyclomatic Complexity Number (highest ccn)  
    - First source code file with the highest ccn
    - Second source code file with the highest ccn
    - Third source code file with the highest ccn
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

# Feedback

The Final Task

- After your experience with the 3D visualizations of JetUML, can you identify where the core of the JetUML project is? Please explain briefly where it's the core and why do you think that this is the core
- Overall, did you find the experiment difficult? Please explain
- What device did you use for the experiment?
    - [ ] Browser
    - [ ] VR device
    - Other
- Do you have any suggestion?