# Analysis of the experiment results

This notebook contains all the analysis made to the set of results for the paper *CodeCity: On-Screen or in Virtual Reality?*


```python
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 12})

vr_results = pd.read_csv("../results/results_vr_formatted.csv")
screen_results = pd.read_csv("../results/results_screen_formatted.csv")
```

--------

## Subject data

In this section we analyze the data of the demographics survey, specifically, the experience of the participants


```python
# Experience level

# VR

df_to_create = vr_results

vr_explvl_df = pd.DataFrame({
    "None":[(df_to_create['EXP_OOP'] == 'None').sum(), (df_to_create['EXP_PRP'] == 'None').sum(), (df_to_create['EXP_FNP'] == 'None').sum(), (df_to_create['EXP_REV'] == 'None').sum(), (df_to_create['EXP_IDE'] == 'None').sum()],
    "Beginner":[(df_to_create['EXP_OOP'] == 'Beginner').sum(), (df_to_create['EXP_PRP'] == 'Beginner').sum(), (df_to_create['EXP_FNP'] == 'Beginner').sum(), (df_to_create['EXP_REV'] == 'Beginner').sum(), (df_to_create['EXP_IDE'] == 'Beginner').sum()],
    "Knowledgeable":[(df_to_create['EXP_OOP'] == 'Knowledgeable').sum(), (df_to_create['EXP_PRP'] == 'Knowledgeable').sum(), (df_to_create['EXP_FNP'] == 'Knowledgeable').sum(), (df_to_create['EXP_REV'] == 'Knowledgeable').sum(), (df_to_create['EXP_IDE'] == 'Knowledgeable').sum()],
    "Advanced":[(df_to_create['EXP_OOP'] == 'Advanced').sum(), (df_to_create['EXP_PRP'] == 'Advanced').sum(), (df_to_create['EXP_FNP'] == 'Advanced').sum(), (df_to_create['EXP_REV'] == 'Advanced').sum(), (df_to_create['EXP_IDE'] == 'Advanced').sum()],
    "Expert":[(df_to_create['EXP_OOP'] == 'Expert').sum(), (df_to_create['EXP_PRP'] == 'Expert').sum(), (df_to_create['EXP_FNP'] == 'Expert').sum(), (df_to_create['EXP_REV'] == 'Expert').sum(), (df_to_create['EXP_IDE'] == 'Expert').sum()]
    }, 
    index=["Exp OOP", "Exp PRP", "Exp FNP", "Exp REV", "Exp IDE"]
)

# Screen

df_to_create = screen_results

screen_explvl_df = pd.DataFrame({
    "None":[(df_to_create['EXP_OOP'] == 'None').sum(), (df_to_create['EXP_PRP'] == 'None').sum(), (df_to_create['EXP_FNP'] == 'None').sum(), (df_to_create['EXP_REV'] == 'None').sum(), (df_to_create['EXP_IDE'] == 'None').sum()],
    "Beginner":[(df_to_create['EXP_OOP'] == 'Beginner').sum(), (df_to_create['EXP_PRP'] == 'Beginner').sum(), (df_to_create['EXP_FNP'] == 'Beginner').sum(), (df_to_create['EXP_REV'] == 'Beginner').sum(), (df_to_create['EXP_IDE'] == 'Beginner').sum()],
    "Knowledgeable":[(df_to_create['EXP_OOP'] == 'Knowledgeable').sum(), (df_to_create['EXP_PRP'] == 'Knowledgeable').sum(), (df_to_create['EXP_FNP'] == 'Knowledgeable').sum(), (df_to_create['EXP_REV'] == 'Knowledgeable').sum(), (df_to_create['EXP_IDE'] == 'Knowledgeable').sum()],
    "Advanced":[(df_to_create['EXP_OOP'] == 'Advanced').sum(), (df_to_create['EXP_PRP'] == 'Advanced').sum(), (df_to_create['EXP_FNP'] == 'Advanced').sum(), (df_to_create['EXP_REV'] == 'Advanced').sum(), (df_to_create['EXP_IDE'] == 'Advanced').sum()],
    "Expert":[(df_to_create['EXP_OOP'] == 'Expert').sum(), (df_to_create['EXP_PRP'] == 'Expert').sum(), (df_to_create['EXP_FNP'] == 'Expert').sum(), (df_to_create['EXP_REV'] == 'Expert').sum(), (df_to_create['EXP_IDE'] == 'Expert').sum()]
    }, 
    index=["Exp OOP", "Exp PRP", "Exp FNP", "Exp REV", "Exp IDE"]
)

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_explvl_df.plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_explvl_df.plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

plt.savefig('explvl.png', bbox_inches='tight')
```


    
![png](output_3_0.png)
    



```python
# Experience Years

# VR

df_to_create = vr_results

vr_explvl_df = pd.DataFrame({
    "<1":[(df_to_create['YEARS_OOP'] == '<1').sum(), (df_to_create['YEARS_PRP'] == '<1').sum(), (df_to_create['YEARS_FNP'] == '<1').sum(), (df_to_create['YEARS_REV'] == '<1').sum(), (df_to_create['YEARS_IDE'] == '<1').sum()],
    "1-3":[(df_to_create['YEARS_OOP'] == '1-3').sum(), (df_to_create['YEARS_PRP'] == '1-3').sum(), (df_to_create['YEARS_FNP'] == '1-3').sum(), (df_to_create['YEARS_REV'] == '1-3').sum(), (df_to_create['YEARS_IDE'] == '1-3').sum()],
    "4-6":[(df_to_create['YEARS_OOP'] == '4-6').sum(), (df_to_create['YEARS_PRP'] == '4-6').sum(), (df_to_create['YEARS_FNP'] == '4-6').sum(), (df_to_create['YEARS_REV'] == '4-6').sum(), (df_to_create['YEARS_IDE'] == '4-6').sum()],
    "7-10":[(df_to_create['YEARS_OOP'] == '7-10').sum(), (df_to_create['YEARS_PRP'] == '7-10').sum(), (df_to_create['YEARS_FNP'] == '7-10').sum(), (df_to_create['YEARS_REV'] == '7-10').sum(), (df_to_create['YEARS_IDE'] == '7-10').sum()],
    "10+":[(df_to_create['YEARS_OOP'] == '10+').sum(), (df_to_create['YEARS_PRP'] == '10+').sum(), (df_to_create['YEARS_FNP'] == '10+').sum(), (df_to_create['YEARS_REV'] == '10+').sum(), (df_to_create['YEARS_IDE'] == '10+').sum()]
    }, 
    index=["Years OOP", "Years PRP", "Years FNP", "Years REV", "Years IDE"]
)

# Screen

df_to_create = screen_results

screen_explvl_df = pd.DataFrame({
    "<1":[(df_to_create['YEARS_OOP'] == '<1').sum(), (df_to_create['YEARS_PRP'] == '<1').sum(), (df_to_create['YEARS_FNP'] == '<1').sum(), (df_to_create['YEARS_REV'] == '<1').sum(), (df_to_create['YEARS_IDE'] == '<1').sum()],
    "1-3":[(df_to_create['YEARS_OOP'] == '1-3').sum(), (df_to_create['YEARS_PRP'] == '1-3').sum(), (df_to_create['YEARS_FNP'] == '1-3').sum(), (df_to_create['YEARS_REV'] == '1-3').sum(), (df_to_create['YEARS_IDE'] == '1-3').sum()],
    "4-6":[(df_to_create['YEARS_OOP'] == '4-6').sum(), (df_to_create['YEARS_PRP'] == '4-6').sum(), (df_to_create['YEARS_FNP'] == '4-6').sum(), (df_to_create['YEARS_REV'] == '4-6').sum(), (df_to_create['YEARS_IDE'] == '4-6').sum()],
    "7-10":[(df_to_create['YEARS_OOP'] == '7-10').sum(), (df_to_create['YEARS_PRP'] == '7-10').sum(), (df_to_create['YEARS_FNP'] == '7-10').sum(), (df_to_create['YEARS_REV'] == '7-10').sum(), (df_to_create['YEARS_IDE'] == '7-10').sum()],
    "10+":[(df_to_create['YEARS_OOP'] == '10+').sum(), (df_to_create['YEARS_PRP'] == '10+').sum(), (df_to_create['YEARS_FNP'] == '10+').sum(), (df_to_create['YEARS_REV'] == '10+').sum(), (df_to_create['YEARS_IDE'] == '10+').sum()]
    }, 
    index=["Years OOP", "Years PRP", "Years FNP", "Years REV", "Years IDE"]
)

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_explvl_df.plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_explvl_df.plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

plt.savefig('expyears.png', bbox_inches='tight')
```


    
![png](output_4_0.png)
    


--------

## Task answers

In this section we analyze the tasks answer of the participants (both VR and Screen).

We have as well the right results (including the top 5 files)


```python
right_answers = pd.read_csv('../references/reference.csv')
```


```python
# Task 1
task1_answers_counts = pd.DataFrame({ 'VR': vr_results['T1_location'].value_counts(), 'Screen': screen_results['T1_location'].value_counts()}).fillna(0).astype(int)
task1_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR</th>
      <th>Screen</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>I found the test module on the West part of the city.</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>In the directory test / ca in the left part of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Left side of the city, under a folder called test/*</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The Test directory is to the west.</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The folder named "test" located on the west side of the city.</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The folder named test/ca at the left of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The test files are located within the area called test, on the west side</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The test folder on the left. /test</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>They are found in the Test folder on the left of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>West side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>West side, test/ca/mcgill/cs</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>left</th>
      <td>12</td>
      <td>0</td>
    </tr>
    <tr>
      <th>on the left side of the welcome message</th>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 1 right answer
right_answers[right_answers['TASK ID'] == 'T1_location']
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>T1_location</td>
      <td>test folder, west zone of the city</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 2
task2_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T2_file1'].value_counts(), 'VR file 2': vr_results['T2_file2'].value_counts(), 'VR file 3': vr_results['T2_file3'].value_counts(), 'Screen file 1': screen_results['T2_file1'].value_counts(), 'Screen file 2': screen_results['T2_file2'].value_counts(), 'Screen file 3': screen_results['T2_file3'].value_counts()}).fillna(0).astype(int)
task2_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>3</td>
      <td>9</td>
      <td>0</td>
      <td>2</td>
      <td>10</td>
      <td>0</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>8</td>
      <td>0</td>
      <td>0</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>1</td>
      <td>3</td>
      <td>7</td>
      <td>0</td>
      <td>2</td>
      <td>10</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 2 right answer
right_answers[right_answers['TASK ID'].isin(['T2_file1', 'T2_file2', 'T2_file3', 'T2_file4', 'T2_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>T2_file1</td>
      <td>JSONObject.java</td>
    </tr>
    <tr>
      <th>2</th>
      <td>T2_file2</td>
      <td>EditorFrame.java</td>
    </tr>
    <tr>
      <th>3</th>
      <td>T2_file3</td>
      <td>SegmentationStyleFactory.java</td>
    </tr>
    <tr>
      <th>4</th>
      <td>T2_file4</td>
      <td>DiagramCanvasController.java</td>
    </tr>
    <tr>
      <th>5</th>
      <td>T2_file5</td>
      <td>SelectionModel.java</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 3
task3_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T3_file1'].value_counts(), 'VR file 2': vr_results['T3_file2'].value_counts(), 'VR file 3': vr_results['T3_file3'].value_counts(), 'Screen file 1': screen_results['T3_file1'].value_counts(), 'Screen file 2': screen_results['T3_file2'].value_counts(), 'Screen file 3': screen_results['T3_file3'].value_counts()}).fillna(0).astype(int)
task3_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ActorNodeViewer.java</th>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>ArrowHeadView.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>MoveTracker.java</th>
      <td>0</td>
      <td>8</td>
      <td>3</td>
      <td>0</td>
      <td>10</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Node.java</th>
      <td>2</td>
      <td>1</td>
      <td>7</td>
      <td>0</td>
      <td>2</td>
      <td>9</td>
    </tr>
    <tr>
      <th>Prototypes.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>SequenceDiagramViewer.java</th>
      <td>10</td>
      <td>2</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 3 right answer
right_answers[right_answers['TASK ID'].isin(['T3_file1', 'T3_file2', 'T3_file3', 'T3_file4', 'T3_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6</th>
      <td>T3_file1</td>
      <td>SequenceDiagramViewer.java</td>
    </tr>
    <tr>
      <th>7</th>
      <td>T3_file2</td>
      <td>MoveTracker.java</td>
    </tr>
    <tr>
      <th>8</th>
      <td>T3_file3</td>
      <td>Node.java</td>
    </tr>
    <tr>
      <th>9</th>
      <td>T3_file4</td>
      <td>Prototypes.java</td>
    </tr>
    <tr>
      <th>10</th>
      <td>T3_file5</td>
      <td>ArrowHeadView.java</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 4
task4_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T4_file1'].value_counts(), 'VR file 2': vr_results['T4_file2'].value_counts(), 'VR file 3': vr_results['T4_file3'].value_counts(), 'Screen file 1': screen_results['T4_file1'].value_counts(), 'Screen file 2': screen_results['T4_file2'].value_counts(), 'Screen file 3': screen_results['T4_file3'].value_counts()}).fillna(0).astype(int)
task4_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ControlFlow.java</th>
      <td>0</td>
      <td>2</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>11</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>2</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 4 right answer
right_answers[right_answers['TASK ID'].isin(['T4_file1', 'T4_file2', 'T4_file3', 'T4_file4', 'T4_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>11</th>
      <td>T4_file1</td>
      <td>JSONObject.java</td>
    </tr>
    <tr>
      <th>12</th>
      <td>T4_file2</td>
      <td>SegmentationStyleFactory.java</td>
    </tr>
    <tr>
      <th>13</th>
      <td>T4_file3</td>
      <td>ControlFlow.java</td>
    </tr>
    <tr>
      <th>14</th>
      <td>T4_file4</td>
      <td>DiagramCanvasController.java</td>
    </tr>
    <tr>
      <th>15</th>
      <td>T4_file5</td>
      <td>JSONTokener.java</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.1
task5_1_answers_counts = pd.DataFrame({ 'VR': vr_results['T5_1_location'].value_counts(), 'Screen': screen_results['T5_1_location'].value_counts()}).fillna(0).astype(int)
task5_1_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR</th>
      <th>Screen</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>In the area called test, west in the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>In the left part of the city in the directory test / co / mcgill / cs</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>On the left side of the welcome message</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The Test directory is to the west.</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The folder called test on the left side of town</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The folder name test is located on the West part of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>The test folder on the left. /Test</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>They are in the"test" folder on the west side of the city.</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Under a folder named test/*, left side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>West side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>left</th>
      <td>12</td>
      <td>0</td>
    </tr>
    <tr>
      <th>test/ca/ on the left of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>test/ca/mcgill/cs/jetuml  at the west side</th>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.1 right answer
right_answers[right_answers['TASK ID'].isin(['T5_1_location'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>16</th>
      <td>T5_1_location</td>
      <td>test folder, west zone of the city</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.2
task5_2_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T5_2_file1'].value_counts(), 'VR file 2': vr_results['T5_2_file2'].value_counts(), 'VR file 3': vr_results['T5_2_file3'].value_counts(), 'Screen file 1': screen_results['T5_2_file1'].value_counts(), 'Screen file 2': screen_results['T5_2_file2'].value_counts(), 'Screen file 3': screen_results['T5_2_file3'].value_counts()}).fillna(0).astype(int)
task5_2_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Diagram.java</th>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>2</td>
      <td>1</td>
      <td>6</td>
      <td>0</td>
      <td>0</td>
      <td>11</td>
    </tr>
    <tr>
      <th>JSONArray.java</th>
      <td>0</td>
      <td>11</td>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>10</td>
      <td>0</td>
      <td>2</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.2 right answer
right_answers[right_answers['TASK ID'].isin(['T5_2_file1', 'T5_2_file2', 'T5_2_file3', 'T5_2_file4', 'T5_2_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>17</th>
      <td>T5_2_file1</td>
      <td>JSONObject.java</td>
    </tr>
    <tr>
      <th>18</th>
      <td>T5_2_file2</td>
      <td>JSONArray.java</td>
    </tr>
    <tr>
      <th>19</th>
      <td>T5_2_file3</td>
      <td>DiagramCanvasController.java</td>
    </tr>
    <tr>
      <th>20</th>
      <td>T5_2_file4</td>
      <td>Diagram.java</td>
    </tr>
    <tr>
      <th>21</th>
      <td>T5_2_file5</td>
      <td>EditorFrame.java</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.3
task5_3_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T5_3_file1'].value_counts(), 'VR file 2': vr_results['T5_3_file2'].value_counts(), 'VR file 3': vr_results['T5_3_file3'].value_counts(), 'Screen file 1': screen_results['T5_3_file1'].value_counts(), 'Screen file 2': screen_results['T5_3_file2'].value_counts(), 'Screen file 3': screen_results['T5_3_file3'].value_counts()}).fillna(0).astype(int)
task5_3_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ArrowHeadView.java</th>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
      <td>9</td>
      <td>0</td>
    </tr>
    <tr>
      <th>ImageCreator.java</th>
      <td>2</td>
      <td>0</td>
      <td>10</td>
      <td>0</td>
      <td>2</td>
      <td>8</td>
    </tr>
    <tr>
      <th>MoveTracker.java</th>
      <td>9</td>
      <td>0</td>
      <td>2</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>PropertySheet.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>ReturnEdge.java</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>StringViewer.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.3 right answer
right_answers[right_answers['TASK ID'].isin(['T5_3_file1', 'T5_3_file2', 'T5_3_file3', 'T5_3_file4', 'T5_3_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>22</th>
      <td>T5_3_file1</td>
      <td>MoveTracker.java</td>
    </tr>
    <tr>
      <th>23</th>
      <td>T5_3_file2</td>
      <td>ArrowHeadView.java</td>
    </tr>
    <tr>
      <th>24</th>
      <td>T5_3_file3</td>
      <td>ImageCreator.java</td>
    </tr>
    <tr>
      <th>25</th>
      <td>T5_3_file4</td>
      <td>StringViewer.java</td>
    </tr>
    <tr>
      <th>26</th>
      <td>T5_3_file5</td>
      <td>ReturnEdge.java</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.4
task5_4_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T5_4_file1'].value_counts(), 'VR file 2': vr_results['T5_4_file2'].value_counts(), 'VR file 3': vr_results['T5_4_file3'].value_counts(), 'Screen file 1': screen_results['T5_4_file1'].value_counts(), 'Screen file 2': screen_results['T5_4_file2'].value_counts(), 'Screen file 3': screen_results['T5_4_file3'].value_counts()}).fillna(0).astype(int)
task5_4_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>-</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>Diagram.java</th>
      <td>2</td>
      <td>0</td>
      <td>8</td>
      <td>0</td>
      <td>0</td>
      <td>11</td>
    </tr>
    <tr>
      <th>JSONArray.java</th>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>10</td>
      <td>0</td>
      <td>2</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5.4 right answer
right_answers[right_answers['TASK ID'].isin(['T5_4_file1', 'T5_4_file2', 'T5_4_file3', 'T5_4_file4', 'T5_4_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>27</th>
      <td>T5_4_file1</td>
      <td>JSONObject.java</td>
    </tr>
    <tr>
      <th>28</th>
      <td>T5_4_file2</td>
      <td>JSONArray.java</td>
    </tr>
    <tr>
      <th>29</th>
      <td>T5_4_file3</td>
      <td>Diagram.java</td>
    </tr>
    <tr>
      <th>30</th>
      <td>T5_4_file4</td>
      <td>SegmentationStyleFactory.java</td>
    </tr>
    <tr>
      <th>31</th>
      <td>T5_4_file5</td>
      <td>JSONTokener.java</td>
    </tr>
  </tbody>
</table>
</div>



--------

## Task Difficulty

In this section we analyze the answers of the participants related to the difficulty of the tasks


```python
vr_dificult_counts = pd.DataFrame({ 'E1 T1 dif': vr_results['T1_dif'].value_counts(), 'E1 T2 dif': vr_results['T2_dif'].value_counts(), 'E1 T3 dif': vr_results['T3_dif'].value_counts(), 'E1 T4 dif': vr_results['T4_dif'].value_counts(), 'E1 T5 dif': vr_results['T5_1_dif'].value_counts(), 'E1 T6 dif': vr_results['T5_2_dif'].value_counts(), 'E1 T7 dif': vr_results['T5_3_dif'].value_counts(), 'E1 T8 dif': vr_results['T5_4_dif'].value_counts()}).fillna(0).astype(int)
dn_row = pd.DataFrame({ 'E1 T1 dif': 0, 'E1 T2 dif': 0, 'E1 T3 dif': 0, 'E1 T4 dif': 0, 'E1 T5 dif': 0, 'E1 T6 dif': 0, 'E1 T7 dif': 0, 'E1 T8 dif': 0 }, index = ["Don't know"])
vr_dificult_counts = pd.concat([vr_dificult_counts, dn_row])
vr_dificult_counts = vr_dificult_counts.reindex(["Agree", "Don't know", "Disagree", "Strongly Disagree"])

screen_dificult_counts = pd.DataFrame({ 'E1 T1 dif': screen_results['T1_dif'].value_counts(), 'E1 T2 dif': screen_results['T2_dif'].value_counts(), 'E1 T3 dif': screen_results['T3_dif'].value_counts(), 'E1 T4 dif': screen_results['T4_dif'].value_counts(), 'E1 T5 dif': screen_results['T5_1_dif'].value_counts(), 'E1 T6 dif': screen_results['T5_2_dif'].value_counts(), 'E1 T7 dif': screen_results['T5_3_dif'].value_counts(), 'E1 T8 dif': screen_results['T5_4_dif'].value_counts()}).fillna(0).astype(int)
screen_dificult_counts = screen_dificult_counts.reindex(["Agree", "Don't know", "Disagree", "Strongly Disagree"])

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_dificult_counts.transpose().plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_dificult_counts.transpose().plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

# Uncomment to save fig
plt.savefig('subdif.png', bbox_inches='tight')
```


    
![png](output_24_0.png)
    


--------

## Task Times

In this section we analyze the time that the participants needed to answer the questions


```python
# Get only the time columns
vr_results_times = vr_results[['T1_dt', 'T2_dt', 'T3_dt', 'T4_dt', 'T5_1_dt', 'T5_2_dt', 'T5_3_dt', 'T5_4_dt']]
screen_results_times = screen_results[['T1_dt', 'T2_dt', 'T3_dt', 'T4_dt', 'T5_1_dt', 'T5_2_dt', 'T5_3_dt', 'T5_4_dt']]

# Add time columns in seconds format in order to print boxplots
vr_results_times['E1 T1 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T1_dt']]).total_seconds()
vr_results_times['E1 T2 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T2_dt']]).total_seconds()
vr_results_times['E1 T3 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T3_dt']]).total_seconds()
vr_results_times['E1 T4 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T4_dt']]).total_seconds()
vr_results_times['E1 T5 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_1_dt']]).total_seconds()
vr_results_times['E1 T6 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_2_dt']]).total_seconds()
vr_results_times['E1 T7 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_3_dt']]).total_seconds()
vr_results_times['E1 T8 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_4_dt']]).total_seconds()

screen_results_times['E1 T1 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T1_dt']]).total_seconds()
screen_results_times['E1 T2 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T2_dt']]).total_seconds()
screen_results_times['E1 T3 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T3_dt']]).total_seconds()
screen_results_times['E1 T4 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T4_dt']]).total_seconds()
screen_results_times['E1 T5 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_1_dt']]).total_seconds()
screen_results_times['E1 T6 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_2_dt']]).total_seconds()
screen_results_times['E1 T7 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_3_dt']]).total_seconds()
screen_results_times['E1 T8 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_4_dt']]).total_seconds()
```

    /tmp/ipykernel_50367/3998310093.py:6: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T1 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T1_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:7: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T2 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T2_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:8: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T3 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T3_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:9: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T4 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T4_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:10: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T5 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_1_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:11: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T6 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_2_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:12: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T7 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_3_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:13: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      vr_results_times['E1 T8 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_4_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:15: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T1 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T1_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:16: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T2 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T2_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:17: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T3 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T3_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:18: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T4 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T4_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:19: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T5 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_1_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:20: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T6 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_2_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:21: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T7 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_3_dt']]).total_seconds()
    /tmp/ipykernel_50367/3998310093.py:22: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      screen_results_times['E1 T8 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_4_dt']]).total_seconds()



```python
# Boxplot for VR
fig = plt.figure(figsize=(10, 6))
plt.subplot(2, 1, 1)
plt.ylim(0,550)
vr_boxplot = vr_results_times.boxplot(column=['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec'], rot=0)
plt.ylabel("VR participant")

# Boxplot for screen
plt.subplot(2, 1, 2)
plt.ylim(0,550)
screen_boxplot = screen_results_times.boxplot(column=['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec'], rot=0)
plt.ylabel("Screen participant")

# Uncomment to save fig
plt.savefig('boxplots_samefigure.png', bbox_inches='tight')
```


    
![png](output_27_0.png)
    



```python
# Get Average table
screen_times_means = screen_results_times[['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec']].mean()
vr_times_means = vr_results_times[['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec']].mean()


table_avgs_raw = pd.DataFrame({'vr': vr_times_means, 'screen': screen_times_means, 'diff': screen_times_means - vr_times_means})
table_avgs_raw.rename(index={'E1 T1 sec': 'T1 avg', 'E1 T2 sec': 'T2 avg', 'E1 T3 sec': 'T3 avg', 'E1 T4 sec': 'T4 avg', 'E1 T5 sec': 'T5.1 avg', 'E1 T6 sec': 'T5.2 avg' ,'E1 T7 sec': 'T5.3 avg', 'E1 T8 sec': 'T5.4 avg'}, inplace=True)
# here is in seconds
table_avgs = table_avgs_raw.round().apply(pd.to_timedelta, unit='s')
table_avgs
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>vr</th>
      <th>screen</th>
      <th>diff</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1 avg</th>
      <td>0 days 00:01:47</td>
      <td>0 days 00:03:57</td>
      <td>0 days 00:02:10</td>
    </tr>
    <tr>
      <th>T2 avg</th>
      <td>0 days 00:01:50</td>
      <td>0 days 00:03:46</td>
      <td>0 days 00:01:56</td>
    </tr>
    <tr>
      <th>T3 avg</th>
      <td>0 days 00:01:42</td>
      <td>0 days 00:03:00</td>
      <td>0 days 00:01:18</td>
    </tr>
    <tr>
      <th>T4 avg</th>
      <td>0 days 00:00:57</td>
      <td>0 days 00:01:38</td>
      <td>0 days 00:00:41</td>
    </tr>
    <tr>
      <th>T5.1 avg</th>
      <td>0 days 00:00:16</td>
      <td>0 days 00:01:00</td>
      <td>0 days 00:00:45</td>
    </tr>
    <tr>
      <th>T5.2 avg</th>
      <td>0 days 00:01:01</td>
      <td>0 days 00:01:56</td>
      <td>0 days 00:00:55</td>
    </tr>
    <tr>
      <th>T5.3 avg</th>
      <td>0 days 00:01:05</td>
      <td>0 days 00:02:11</td>
      <td>0 days 00:01:06</td>
    </tr>
    <tr>
      <th>T5.4 avg</th>
      <td>0 days 00:00:28</td>
      <td>0 days 00:01:12</td>
      <td>0 days 00:00:44</td>
    </tr>
  </tbody>
</table>
</div>




```python
from string import Template

class DeltaTemplate(Template):
    delimiter = "%"
    
def strfdelta(tdelta, fmt):
    d = {}
    d["H"], rem = divmod(tdelta.seconds, 3600)
    d["M"], d["S"] = divmod(rem, 60)
    t = DeltaTemplate(fmt)
    return t.substitute(**d)

# Friendly table

table_avgs['vr'] = table_avgs['vr'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs['screen'] = table_avgs['screen'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs['diff'] = table_avgs['diff'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>vr</th>
      <th>screen</th>
      <th>diff</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1 avg</th>
      <td>1:47</td>
      <td>3:57</td>
      <td>2:10</td>
    </tr>
    <tr>
      <th>T2 avg</th>
      <td>1:50</td>
      <td>3:46</td>
      <td>1:56</td>
    </tr>
    <tr>
      <th>T3 avg</th>
      <td>1:42</td>
      <td>3:0</td>
      <td>1:18</td>
    </tr>
    <tr>
      <th>T4 avg</th>
      <td>0:57</td>
      <td>1:38</td>
      <td>0:41</td>
    </tr>
    <tr>
      <th>T5.1 avg</th>
      <td>0:16</td>
      <td>1:0</td>
      <td>0:45</td>
    </tr>
    <tr>
      <th>T5.2 avg</th>
      <td>1:1</td>
      <td>1:56</td>
      <td>0:55</td>
    </tr>
    <tr>
      <th>T5.3 avg</th>
      <td>1:5</td>
      <td>2:11</td>
      <td>1:6</td>
    </tr>
    <tr>
      <th>T5.4 avg</th>
      <td>0:28</td>
      <td>1:12</td>
      <td>0:44</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Total AVG

vr_secs_df = vr_results_times[['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec']]
# vr_secs_df["Total"] = vr_secs_df.sum(axis=1)
total_vr = vr_secs_df.values.mean()
print("VR all avg: {}".format(strfdelta(pd.to_timedelta(total_vr, unit='s'), '%M:%S')))

screen_secs_df = screen_results_times[['E1 T1 sec', 'E1 T2 sec', 'E1 T3 sec', 'E1 T4 sec', 'E1 T5 sec', 'E1 T6 sec', 'E1 T7 sec', 'E1 T8 sec']]
# screen_secs_df["Total"] = screen_secs_df.sum(axis=1)
total_screen = screen_secs_df.values.mean()
print("Screen all avg: {}".format(strfdelta(pd.to_timedelta(total_screen, unit='s'), '%M:%S')))

```

    VR all avg: 1:8
    Screen all avg: 2:20



```python
from scipy.stats import mannwhitneyu

mannwhitney_df = pd.DataFrame(columns=["U", "p-value"])

# Task 1
stat, p = mannwhitneyu(screen_results_times['E1 T1 sec'].values, vr_results_times['E1 T1 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T1"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 2
stat, p = mannwhitneyu(screen_results_times['E1 T2 sec'].values, vr_results_times['E1 T2 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T2"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 3
stat, p = mannwhitneyu(screen_results_times['E1 T3 sec'].values, vr_results_times['E1 T3 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T3"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 4
stat, p = mannwhitneyu(screen_results_times['E1 T4 sec'].values, vr_results_times['E1 T4 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T4"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 5.1
stat, p = mannwhitneyu(screen_results_times['E1 T5 sec'].values, vr_results_times['E1 T5 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T5.1"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 5.2
stat, p = mannwhitneyu(screen_results_times['E1 T6 sec'].values, vr_results_times['E1 T6 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T5.2"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 5.3
stat, p = mannwhitneyu(screen_results_times['E1 T7 sec'].values, vr_results_times['E1 T7 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T5.3"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 5.4
stat, p = mannwhitneyu(screen_results_times['E1 T8 sec'].values, vr_results_times['E1 T8 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T5.4"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# All
screen_results_times_concat = pd.concat([screen_results_times['E1 T1 sec'], screen_results_times['E1 T2 sec'], screen_results_times['E1 T3 sec'], screen_results_times['E1 T4 sec'], screen_results_times['E1 T5 sec'], screen_results_times['E1 T6 sec'],screen_results_times['E1 T7 sec'], screen_results_times['E1 T8 sec']])
vr_results_times_concat = pd.concat([vr_results_times['E1 T1 sec'], vr_results_times['E1 T2 sec'], vr_results_times['E1 T3 sec'], vr_results_times['E1 T4 sec'], vr_results_times['E1 T5 sec'], vr_results_times['E1 T6 sec'],vr_results_times['E1 T7 sec'], vr_results_times['E1 T8 sec']])
stat, p = mannwhitneyu(screen_results_times_concat.values, vr_results_times_concat.values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["All"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Show table
mannwhitney_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>U</th>
      <th>p-value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1</th>
      <td>128.0</td>
      <td>0.001351</td>
    </tr>
    <tr>
      <th>T2</th>
      <td>129.0</td>
      <td>0.001081</td>
    </tr>
    <tr>
      <th>T3</th>
      <td>137.0</td>
      <td>0.000195</td>
    </tr>
    <tr>
      <th>T4</th>
      <td>120.0</td>
      <td>0.006088</td>
    </tr>
    <tr>
      <th>T5.1</th>
      <td>144.0</td>
      <td>0.000035</td>
    </tr>
    <tr>
      <th>T5.2</th>
      <td>138.0</td>
      <td>0.000154</td>
    </tr>
    <tr>
      <th>T5.3</th>
      <td>128.5</td>
      <td>0.001209</td>
    </tr>
    <tr>
      <th>T5.4</th>
      <td>144.0</td>
      <td>0.000036</td>
    </tr>
    <tr>
      <th>All</th>
      <td>7531.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Cliff Effect Size

from __future__ import division

def cliffsDelta(lst1,lst2,
                dull = [0.147, # small
                        0.33,  # medium
                        0.474 # large
                        ][0] ): 
  "Returns true if there are more than 'dull' differences"
  m, n = len(lst1), len(lst2)
  lst2 = sorted(lst2)
  j = more = less = 0
  for repeats,x in runs(sorted(lst1)):
    while j <= (n - 1) and lst2[j] <  x: 
      j += 1
    more += j*repeats
    while j <= (n - 1) and lst2[j] == x: 
      j += 1
    less += (n - j)*repeats
  d= (more - less) / (m*n) 
  return abs(d), abs(d)  > dull
  
def runs(lst):
  "Iterator, chunks repeated values"
  for j,two in enumerate(lst):
    if j == 0:
      one,i = two,0
    if one!=two:
      yield j - i,one
      i = j
    one=two
  yield j - i + 1,two

# Cliff Effect Size Dataframe
cliff_df = pd.DataFrame(columns=["Cliff Eff Size"])

# Task 1
stat, isdif = cliffsDelta(screen_results_times['E1 T1 sec'].values, vr_results_times['E1 T1 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T1"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 2
stat, isdif = cliffsDelta(screen_results_times['E1 T2 sec'].values, vr_results_times['E1 T2 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T2"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 3
stat, isdif = cliffsDelta(screen_results_times['E1 T3 sec'].values, vr_results_times['E1 T2 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T3"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 4
stat, isdif = cliffsDelta(screen_results_times['E1 T4 sec'].values, vr_results_times['E1 T4 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T4"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 5.1
stat, isdif = cliffsDelta(screen_results_times['E1 T5 sec'].values, vr_results_times['E1 T5 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T5"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 5.2
stat, isdif = cliffsDelta(screen_results_times['E1 T6 sec'].values, vr_results_times['E1 T6 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T6"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 5.3
stat, isdif = cliffsDelta(screen_results_times['E1 T7 sec'].values, vr_results_times['E1 T7 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T7"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 5.4
stat, isdif = cliffsDelta(screen_results_times['E1 T8 sec'].values, vr_results_times['E1 T8 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T8"])
cliff_df = pd.concat([cliff_df, dn_row])

# All
stat, isdif = cliffsDelta(screen_results_times_concat.values, vr_results_times_concat.values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["All"])
cliff_df = pd.concat([cliff_df, dn_row])

# Show table
cliff_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cliff Eff Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1</th>
      <td>0.777778</td>
    </tr>
    <tr>
      <th>T2</th>
      <td>0.791667</td>
    </tr>
    <tr>
      <th>T3</th>
      <td>0.75</td>
    </tr>
    <tr>
      <th>T4</th>
      <td>0.666667</td>
    </tr>
    <tr>
      <th>T5</th>
      <td>1.0</td>
    </tr>
    <tr>
      <th>T6</th>
      <td>0.916667</td>
    </tr>
    <tr>
      <th>T7</th>
      <td>0.784722</td>
    </tr>
    <tr>
      <th>T8</th>
      <td>1.0</td>
    </tr>
    <tr>
      <th>All</th>
      <td>0.634332</td>
    </tr>
  </tbody>
</table>
</div>




```python

```
