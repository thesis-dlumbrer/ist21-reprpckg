###############################################
# Questionnaire presented to the participants #
###############################################

###############################################
## Pre-Experiment Questionnaire: Demographics

- How old are you?

- Job position (e.g., developer, researcher, project manager)

- Experience level in Programming
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert

- Experience level in using an IDE (Pycharm, Eclipse, Visual Studio, etc.)
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert

- Programming experience (in years)
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10

- How long have you been using an IDE (in years)?
    - [ ] Less than 1
    - [ ] 1-3
    - [ ] 4-6
    - [ ] 7-10
    - [ ] 10

- Familiarity with the JetUML system (https://github.com/prmr/JetUML)?
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert

- Experience with Software Visualization
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert

- Experience with VR devices (*This is only presented to VR participants)
    - [ ] None
    - [ ] Beginner
    - [ ] Knowledgeable
    - [ ] Advanced
    - [ ] Expert

###############################################
## Training

You are going to do a training to get used to the A-Frame environment (and VR environment). 
Please follow the instructions on the training page and take your time exploring the scenes.
Let the supervisor know if you have any question or problem.

- VR participants:
    1. First Steps from Oculus Quest 2 glasses (Optional)
    2. A-Frame mockup scene for training: `scenes/vr/training.html`

- Screen participants:
    1. A-Frame mockup scene for training: `scenes/screen/training.html`

###############################################
## Tasks

You are going to visit a city that represents the JetUML project (https://github.com/prmr/JetUML) as of March 24, 2021.

Scene: `scenes/(screen or vr)/jetuml2021.html`

Each building represents a file of the system. Each district represents a folder. Districts can be nested to represent nested folders. On each building, we mapped the values of three software metrics:

- Area: number of functions (num_funs) of the code file. 
- Height: Lines of Code (LOC) per function. 
- Color*: Cyclomatic Complexity Number (see https://en.wikipedia.org/wiki/Cyclomatic_complexity)

* The color follows a heatmap scale from blue (low) to red (high).

- **Task 1**: Explore the city to locate all the test code (i.e., files and directories) of the system
    - Where is the location of the test files? (e.g., "The folder named "junit" that is located on the East side of the city)
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 2**: Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system
    - First source code file with the highest number of functions
    - Second source code file with the highest number of functions
    - Third source code file with the highest number of functions
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 3**: Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system
    - First source code file with the highest lines of code per function
    - Second source code file with the highest lines of code per function
    - Third source code file with the highest lines of code per function
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 4**: The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system.
    - First source code file with the highest lines of code
    - Second source code file with the highest lines of code
    - Third source code file with the highest lines of code
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 5**: Find the three source code files (not testing files) with the highest Cyclomatic Complexity Number (highest ccn)  
    - First source code file with the highest ccn
    - Second source code file with the highest ccn
    - Third source code file with the highest ccn
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 6**: Find the source code file (not testing files) with the highest lines of code per function (highest loc_per_function) inside the src/ca/mcgill/cs/jetuml/diagram/nodes folder.
    - Source code file with the highest lines of code per function
    - Comment on the following sentence: "The task was difficult"
        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- **Task 7**: After your experience with the 3D visualizations of JetUML, can you identify where the core of the JetUML project is? Please explain where the core is located and why do you think that it is the core.

###############################################
# Post-Experiment Questionnaire: Feedback

- Overall, did you find the experiment difficult? 

        - [ ] Strongly Agree
        - [ ] Agree
        - [ ] Don't know
        - [ ] Disagree
        - [ ] Strongly Disagree

- Please explain

- Do you have any suggestions?