# Response mode

- All the participants must answer out loud. The supervisor must note the answer (the name of the building) and the metric value.


# During the tasks

- Ask the participant to notice the supervisor when she/he starts reading the tasks and finished it (for a fair time comparison).
