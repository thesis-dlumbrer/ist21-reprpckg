# CodeCity: A Comparison of On-Screen and Virtual Reality - Replication package

All the material for replicating the experiment and analysis

## Scenes for the experiments

[Go here to see the scenes](https://thesis-dlumbrer.gitlab.io/ist21-reprpckg)


## Experiments

This study contains two controlled experiments, each one was conducted independently, the following links redirect to the information, scenes, results, and analysis of each experiment:

- [First experiment](./first_experiment/README.md)
- [Second experiment](./second_experiment/README.md)
